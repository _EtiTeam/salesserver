﻿using NServiceBus;
using NServiceBus.Logging;
using ServiceBusDLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using NServiceBus.Persistence.Sql;

namespace SalesServerStarter.ServiceBus.Order
{
    class OrderSaga :
        SqlSaga<OrderSagaData>,
        IAmStartedByMessages<StartOrder>,
        IHandleMessages<CheckOrderPaymentResponse>,
        IHandleMessages<OrderItemReserved>,
        IHandleMessages<OrderProviderIsReady>,
        IHandleTimeouts<SimpleTimeout>
    {
        static ILog log = LogManager.GetLogger<OrderSaga>();

        protected override string CorrelationPropertyName => nameof(Data.Id);


        protected override void ConfigureMapping(IMessagePropertyMapper mapper)
        {

            mapper.ConfigureMapping<MessageBase>(message => message.Id);
        }

        //protected override void ConfigureHowToFindSaga(SagaPropertyMapper<OrderSagaData> mapper)
        //{
        //    // it configure all messages
            
        //}

        public Task Handle(StartOrder message, IMessageHandlerContext context)
        {
            log.Info($"Saga: {Data.Id} ||| Fetched StartOrder: { message.Id }");
            Data.OrderId = message.OrderId;

            return context.Send(ServiceBusConstants.ACCOUNTANCY_SERVICE_NAME,
                new CheckOrderPayment
                {
                    Id = Data.Id,
                    OrderId = Data.OrderId 
                });
        }

        public Task Handle(CheckOrderPaymentResponse message, IMessageHandlerContext context)
        {
            log.Info($"Saga: {Data.Id} ||| Fetched CheckPaymentResponse: { message.Id }");

            context.Publish(new CommitedOrder { Id = Data.Id });
            var timeout = TimeSpan.FromSeconds(10);
            return RequestTimeout(context, timeout, new SimpleTimeout { Id = Data.Id });
        }

        public Task Handle(OrderItemReserved message, IMessageHandlerContext context)
        {
            log.Info($"Saga: {Data.Id} ||| Fetched OrderItemReserved: { message.Id }");

            Data.OrderItemReservedMessageArrived = true;
            checkIfCommitedOrderIsComplete();
            return Task.CompletedTask;
        }

        public Task Handle(OrderProviderIsReady message, IMessageHandlerContext context)
        {
            log.Info($"Saga: { Data.Id } ||| Fetched OrderProviderIsReady: { message.Id }");

            Data.OrderProviderIsReadyMessageArrived = true;
            checkIfCommitedOrderIsComplete();
            return Task.CompletedTask;
        }

        public Task Timeout(SimpleTimeout state, IMessageHandlerContext context)
        {
            log.Info($"Saga: { Data.Id } ||| TIMEOUT: { state.Id }");

            MarkAsComplete();
            return Task.CompletedTask;
        }

        private void checkIfCommitedOrderIsComplete()
        {
            if (Data.OrderItemReservedMessageArrived && Data.OrderProviderIsReadyMessageArrived)
            {
                log.Info($"Saga: { Data.Id } has been marked as completed");

                MarkAsComplete();
                // TODO 
            }
        }

       
    }
}
