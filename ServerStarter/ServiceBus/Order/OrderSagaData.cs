﻿using System;
using NServiceBus;

namespace SalesServerStarter.ServiceBus.Order
{
    class OrderSagaData : ContainSagaData
    {
        override public Guid Id { get; set; }
        public int OrderId { get; set; }

        public bool OrderItemReservedMessageArrived { get; set; } = false;
        public bool OrderProviderIsReadyMessageArrived { get; set; } = false;
        public Guid? RecentyConfirmationId { get; set; }
    }
}
