﻿using NServiceBus;
using ServiceBusDLL;
using SalesService;

namespace SalesServerStarter.ServiceBus
{
    static public class SalesEvents
    {
        private static IEndpointInstance busEndpoint;
        public static void initialize(IEndpointInstance endpoint) // it also call static constructor
        {
            busEndpoint = endpoint;
        }

        static SalesEvents()
        {
            // set event as action in delegates
            SalesDelegates.AddOnOrderCreatedAction(onOrderCreated);
        }

        public static void onOrderCreated(int orderId)
        {
            busEndpoint.SendLocal<StartOrder>(x => x.OrderId = orderId);
        }
    }
}
