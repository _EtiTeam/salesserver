﻿using System.ServiceModel;
using System.ServiceModel.Description;

namespace SalesService
{
    public static class UserServiceConnector
    {
        // interface must be dispose
        public static UserServiceDll.IUserService createInstance() {
            var f = new ChannelFactory<UserServiceDll.IUserService>(new WebHttpBinding(),
                new EndpointAddress("http://localhost/salesService/users/api"));
            f.Endpoint.Behaviors.Add(new WebHttpBehavior());
            var c = f.CreateChannel();
            //((IDisposable)c).Dispose();
            return c;
        }
    }
}