﻿using System;
using System.Data.SqlClient;

namespace SalesService
{
    public class UserService : UserServiceDll.UserServiceImpl
    {
        public override SqlConnection SqlConnection =>
            new DatabaseConnection().SqlConnection;

        public override string baseGuiPath => 
            ServerConfiguration.BASE_GUI_PATH;
    }
}
