﻿using ModelDLL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.ServiceModel.Web;
using System.Text;

namespace SalesService
{
    public class SalesService : ISalesService, IRestSalesService
    {
        public static void initialize()
        {
            new SalesService(); // for calling constructor
        }

        public SalesService()
        {
            createTablesIfNotExist();
        }

        private void createTablesIfNotExist()
        {
            bool tablesExists = true;
            if (ModelOperations.CheckIfTableExists(Order.TableName, new DatabaseConnection()) == false) tablesExists = false;
            if (ModelOperations.CheckIfTableExists(Book.TableName, new DatabaseConnection()) == false) tablesExists = false;
            if (ModelOperations.CheckIfTableExists(Customer.TableName, new DatabaseConnection()) == false) tablesExists = false;
            
            if (tablesExists == false)
            {
                recreateTables();
            }
        }

        public void redirection()
        {
            var ctx = WebOperationContext.Current.OutgoingResponse;
            ctx.Location = "rest/index.html";
            ctx.StatusCode = System.Net.HttpStatusCode.Redirect;
        }

        public void recreateTables()
        {
            //drop tables
            ModelOperations.DropTableIfExists(Order.TableName, new DatabaseConnection());
            ModelOperations.DropTableIfExists(Book.TableName, new DatabaseConnection());
            ModelOperations.DropTableIfExists(Customer.TableName, new DatabaseConnection());

            //create tables
            ModelOperations.ExecuteSqlCommand(Customer.CreateCommand, new DatabaseConnection());
            ModelOperations.ExecuteSqlCommand(Book.CreateCommand, new DatabaseConnection());
            ModelOperations.ExecuteSqlCommand(Order.CreateCommand, new DatabaseConnection());

            //insert mock values into tables
            ModelOperations.ExecuteSqlCommand(Customer.mockValuesCommand, new DatabaseConnection());
            ModelOperations.ExecuteSqlCommand(Book.mockValuesCommand, new DatabaseConnection());
            ModelOperations.ExecuteSqlCommand(Order.mockValuesCommand, new DatabaseConnection());

            ModelOperations.ExecuteSqlCommand("DELETE FROM SalesService_OrderSaga", new ServiceBusDLL.SagasDatabaseConnection());
        }

        private bool isAuthentificated()
        {
            bool isAuthentificated = false;
            WebOperationContext webOperationContext = WebOperationContext.Current;
            if (webOperationContext != null)
            {
                var sessionUUID = webOperationContext.IncomingRequest.Headers.Get("Authorization");

                if (sessionUUID != null)
                {

                    sessionUUID = sessionUUID.Replace("\\", "").Replace("\"", "");
                    try
                    {
                        var userService = UserServiceConnector.createInstance();
                        using ((System.IDisposable)userService)
                        {
                            isAuthentificated = userService.isSignedIn(Guid.Parse(sessionUUID));
                        }
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }
                }

            }
            return isAuthentificated;

        }

        public Stream getJsFile(string jsFileName)
        {
            return getStreamFromFile(ServerConfiguration.BASE_GUI_PATH + "assets\\js\\" + jsFileName, GuiDLL.GuiConstants.JS_TYPE);
        }

        public Stream getCssFile(string cssFile)
        {
            return getStreamFromFile(ServerConfiguration.BASE_GUI_PATH + "assets\\css\\" + cssFile, GuiDLL.GuiConstants.CSS_TYPE);
        }

        public Stream getImageFile(string imageFile)
        {
            return getStreamFromFile(ServerConfiguration.BASE_GUI_PATH + "images\\" + imageFile, GuiDLL.GuiConstants.IMG_TYPE);
        }

        public Stream getFont1()
        {
            return getStreamFromFile(ServerConfiguration.BASE_GUI_PATH + "assets\\fonts\\fontawesome-webfont.woff", "application/font-woff");
        }

        public Stream getFont2()
        {
            return getStreamFromFile(ServerConfiguration.BASE_GUI_PATH + "assets\\fonts\\fontawesome-webfont.woff2", "application/font-woff2");
        }

        public Stream getFont3()
        {
            return getStreamFromFile(ServerConfiguration.BASE_GUI_PATH + "assets\\fonts\\fontawesome-webfont.ttf", "application/x-font-ttf");
        }

        public Stream getIndexPage()
        {
            if (!isAuthentificated())
                return getLoginPage();
            else
                return getStreamFromFile(ServerConfiguration.BASE_GUI_PATH + "index.html", GuiDLL.GuiConstants.HTML_TYPE);
        }

        public Stream getSubPage()
        {
            if (!isAuthentificated())
                return getLoginPage();
            else
                return getStreamFromFile(ServerConfiguration.BASE_GUI_PATH + "subpage.html", GuiDLL.GuiConstants.HTML_TYPE);
        }
        public Stream getLoginPage()
        {
            return getStreamFromFile(ServerConfiguration.BASE_GUI_PATH + "login.html", GuiDLL.GuiConstants.HTML_TYPE);
        }
        private static Stream getStreamFromFile(string filePath, string contentType)
        {
            var ctx = WebOperationContext.Current.IncomingRequest;

            Console.WriteLine("[" + DateTime.Now.ToLongTimeString() + "] " + ctx.UriTemplateMatch.RequestUri.ToString());
            OutgoingWebResponseContext context =
                WebOperationContext.Current.OutgoingResponse;
            context.ContentType = contentType;
            try
            {
                byte[] file = File.ReadAllBytes(filePath);
                return new MemoryStream(file);
            }
            catch (FileNotFoundException fileNotFoundException)
            {
                return new MemoryStream(ASCIIEncoding.UTF8.GetBytes($"File not found. Description: {fileNotFoundException.Message}"));
            }
        }

        public IList<ModelDLL.Book> getBooksSelect()
        {
            if (!isAuthentificated())
                return null;
            else
                return ModelOperations.Select<Book>(Book.SelectCommand, new DatabaseConnection());
        }

        public IList<Customer> getCustomersSelect()
        {
            if (!isAuthentificated())
                return null;
            else
                return ModelOperations.Select<Customer>(Customer.SelectCommand, new DatabaseConnection());
        }

        public IList<Order> getOrdersSelect()
        {
            if (!isAuthentificated())
                return null;
            else
                return ModelOperations.Select<Order>(Order.SelectCommand, new DatabaseConnection());
        }

        public void postBooksInsert(System.IO.Stream s)
        {
            if (!isAuthentificated())
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                return;
            }
            else
            {
                Book book = ModelOperations.GetFromJson<Book>(s).FirstOrDefault();
                try
                {
                    ModelOperations.Insert(book, new DatabaseConnection());
                }
                catch (SqlException ex)
                {
                    WebOperationContext ctx = WebOperationContext.Current;
                    ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Conflict;
                    ctx.OutgoingResponse.StatusDescription = ex.Message;
                }
            }
        }

        public void postCustomersInsert(System.IO.Stream s)
        {
            if (!isAuthentificated())
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                return;
            }
            else
            {
                Customer customer = ModelOperations.GetFromJson<Customer>(s).FirstOrDefault();
                try
                {
                    ModelOperations.Insert(customer, new DatabaseConnection());
                }
                catch (SqlException ex)
                {
                    WebOperationContext ctx = WebOperationContext.Current;
                    ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Conflict;
                    ctx.OutgoingResponse.StatusDescription = ex.Message;
                }
            }
        }


        public void postOrdersInsert(System.IO.Stream s)
        {
            if (!isAuthentificated())
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                return;
            }
            else
            {
                Order order = ModelOperations.GetFromJson<Order>(s).FirstOrDefault();
                try
                {
                    ModelOperations.Insert(order, new DatabaseConnection());
                    SalesDelegates.InvokeOnOrderCreated(order.Id.Value);
                }
                catch (SqlException ex)
                {
                    WebOperationContext ctx = WebOperationContext.Current;
                    ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Conflict;
                    ctx.OutgoingResponse.StatusDescription = ex.Message;
                }
            }
        }


        public void getTableDelete(string tableName, string id)
        {
            if (!isAuthentificated())
            {
                return;
            }
            try
            {
                ModelOperations.ExecuteSqlCommand($"delete from {tableName} where ID='{id}'", new DatabaseConnection());

            }
            catch (SqlException ex)
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Conflict;
                ctx.OutgoingResponse.StatusDescription = ex.Message;
            }
        }

        public void postBooksUpdate(Stream s)
        {
            if (!isAuthentificated())
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                return;
            }
            else
            {
                Book book = ModelOperations.GetFromJson<Book>(s).FirstOrDefault();
                try
                {
                    ModelOperations.Update(book, new DatabaseConnection());
                }
                catch (SqlException ex)
                {
                    WebOperationContext ctx = WebOperationContext.Current;
                    ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Conflict;
                    ctx.OutgoingResponse.StatusDescription = ex.Message;
                }
            }

        }

        public void postCustomersUpdate(Stream s)
        {
            if (!isAuthentificated())
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                return;
            }
            else
            {
                Customer customer = ModelOperations.GetFromJson<Customer>(s).FirstOrDefault();
                try
                {
                    ModelOperations.Update(customer, new DatabaseConnection());
                }
                catch (SqlException ex)
                {
                    WebOperationContext ctx = WebOperationContext.Current;
                    ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Conflict;
                    ctx.OutgoingResponse.StatusDescription = ex.Message;
                }
            }

        }

        public void postOrdersUpdate(Stream s)
        {
            if (!isAuthentificated())
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                return;
            }
            else
            {
                Order order = ModelOperations.GetFromJson<Order>(s).FirstOrDefault();
                try
                {
                    ModelOperations.Update(order, new DatabaseConnection());
                    SalesDelegates.InvokeOnOrderCreated(order.Id.Value);
                }
                catch (SqlException ex)
                {
                    WebOperationContext ctx = WebOperationContext.Current;
                    ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Conflict;
                    ctx.OutgoingResponse.StatusDescription = ex.Message;
                }
            }

        }
    }
}
    
