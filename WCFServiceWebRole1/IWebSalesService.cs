﻿using System.ServiceModel;
using System.ServiceModel.Web;

namespace SalesService
{
    [ServiceContract]
    public interface ISalesService
    {
        [OperationContract]
        [WebGet(UriTemplate = "")]
        void redirection();
    }
}
