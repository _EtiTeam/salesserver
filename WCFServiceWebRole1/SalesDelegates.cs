﻿using System;

namespace SalesService
{
    public static class SalesDelegates
    {
        private static volatile Action<int> onOrderCreated = x => { }; // empty delegate

        public static void InvokeOnOrderCreated(int orderId)
        {
            lock (onOrderCreated)
            {
                onOrderCreated(orderId);
            }
        }

        public static void AddOnOrderCreatedAction(Action<int> method)
        {
            lock (onOrderCreated)
            {
                onOrderCreated += method;
            }
        }

        public static void RemoveOnOrderCreatedAction(Action<int> method)
        {
            lock (onOrderCreated)
            {
                onOrderCreated -= method;
            }
        }
    }
}