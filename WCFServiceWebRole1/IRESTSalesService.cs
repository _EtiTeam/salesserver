﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace SalesService
{
    [ServiceContract]
    public interface IRestSalesService
    {
        //Main Web Resources 
        [OperationContract]
        [WebGet(UriTemplate = "assets/js/{jsFile}")]
        System.IO.Stream getJsFile(string jsFile);

        [OperationContract]
        [WebGet(UriTemplate = "assets/css/{cssFile}")]
        System.IO.Stream getCssFile(string cssFile);

        [OperationContract]
        [WebGet(UriTemplate = "images/{imageFile}")]
        System.IO.Stream getImageFile(string imageFile);

        [OperationContract]
        [WebGet(UriTemplate = "assets/fonts/fontawesome-webfont.woff?v=4.6.3")]
        System.IO.Stream getFont1();

        [OperationContract]
        [WebGet(UriTemplate = "assets/fonts/fontawesome-webfont.woff2?v=4.6.3")]
        System.IO.Stream getFont2();

        [OperationContract]
        [WebGet(UriTemplate = "assets/fonts/fontawesome-webfont.ttf?v=4.6.3")]
        System.IO.Stream getFont3();

        //Sales Web Resources
        [OperationContract]
        [WebGet(UriTemplate = "index.html")]
        System.IO.Stream getIndexPage();

        [OperationContract]
        [WebGet(UriTemplate = "subpage.html")]
        System.IO.Stream getSubPage();

        [OperationContract]
        [WebGet(
            UriTemplate = "Books/select",
            ResponseFormat = WebMessageFormat.Json
        )]
        IList<ModelDLL.Book> getBooksSelect();

        [OperationContract]
        [WebGet(
           UriTemplate = "Customers/select",
           ResponseFormat = WebMessageFormat.Json
       )]
        IList<ModelDLL.Customer> getCustomersSelect();

        [OperationContract]
        [WebGet(
           UriTemplate = "Orders/select",
           ResponseFormat = WebMessageFormat.Json
       )]

        IList<ModelDLL.Order> getOrdersSelect();

        [OperationContract]
        [WebInvoke(
            UriTemplate = "Books/insert",
            ResponseFormat = WebMessageFormat.Json
        )]
        void postBooksInsert(System.IO.Stream s);

        [OperationContract]
        [WebInvoke(
           UriTemplate = "Customers/insert",
           ResponseFormat = WebMessageFormat.Json
       )]
        void postCustomersInsert(System.IO.Stream s);

        [OperationContract]
        [WebInvoke(
           UriTemplate = "Orders/insert",
           ResponseFormat = WebMessageFormat.Json
       )]
        void postOrdersInsert(System.IO.Stream s);

        [OperationContract]
        [WebInvoke(
            UriTemplate = "Books/update",
            ResponseFormat = WebMessageFormat.Json
        )]
        void postBooksUpdate(System.IO.Stream s);

        [OperationContract]
        [WebInvoke(
           UriTemplate = "Customers/update",
           ResponseFormat = WebMessageFormat.Json
       )]
        void postCustomersUpdate(System.IO.Stream s);

        [OperationContract]
        [WebInvoke(
           UriTemplate = "Orders/update",
           ResponseFormat = WebMessageFormat.Json
       )]
        void postOrdersUpdate(System.IO.Stream s);

        [OperationContract]
        [WebGet(
            UriTemplate = "{tableName}/delete/{id}",
            ResponseFormat = WebMessageFormat.Json
        )]
        void getTableDelete(string tableName, string id);

        [OperationContract]
        [WebGet(UriTemplate = "rt")]
        void recreateTables();
    }
}
