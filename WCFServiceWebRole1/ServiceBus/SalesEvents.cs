﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NServiceBus;
using ServiceBusDLL;

namespace SalesService.ServiceBus
{
    static public class SalesEvents
    {
        private static IEndpointInstance busEndpoint;
        public static void initialize(IEndpointInstance endpoint) // it also call static constructor
        {
            busEndpoint = endpoint;
        }

        static SalesEvents()
        {
            // set event as action in delegates
            SalesDelegates.AddOnOrderCreatedAction(onOrderCreated);
        }

        public static void onOrderCreated(int orderId)
        {
            busEndpoint.SendLocal<StartOrder>(x => x.OrderId = orderId);
        }
    }
}
