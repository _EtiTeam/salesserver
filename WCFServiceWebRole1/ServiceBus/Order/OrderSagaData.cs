﻿using System;
using NServiceBus;

namespace SalesService.ServiceBus.Order
{
    class OrderSagaData : ContainSagaData
    {
        override public Guid Id { get; set; }
        public int OrderId { get; set; }

        public bool ItemReservedMessageArrived { get; set; } = false;
        public bool ProviderIsReadyMessageArrived { get; set; } = false;
        public bool IsItemReserved { get; set; } = false;
        public bool IsProviderReady { get; set; } = false;

        public Guid? RecentlyReservationId { get; set; }
        public Guid? RetryReservationId { get; set; } // TODO: to remove #99
    }
}
