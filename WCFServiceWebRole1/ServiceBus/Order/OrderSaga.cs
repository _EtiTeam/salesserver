﻿using NServiceBus;
using NServiceBus.Logging;
using ServiceBusDLL;
using System;
using System.Threading.Tasks;
using NServiceBus.Persistence.Sql;

namespace SalesService.ServiceBus.Order
{
    class OrderSaga :
        SqlSaga<OrderSagaData>,
        IAmStartedByMessages<StartOrder>,
        IHandleMessages<CheckOrderPaymentResponse>,
        IHandleMessages<OrderItemReserved>,
        IHandleMessages<OrderItemNotReserved>,
        IHandleMessages<OrderProviderIsReady>,
        IHandleMessages<OrderProviderIsNotReady>,
        IHandleTimeouts<RetryConfirmationOrderTimeout>
    {
        static ILog log = LogManager.GetLogger<OrderSaga>();

        protected override string CorrelationPropertyName => nameof(Data.Id);
                       
        protected override void ConfigureMapping(IMessagePropertyMapper mapper)
        {
            mapper.ConfigureMapping<MessageBase>(message => message.Id);
        }

        public Task Handle(StartOrder message, IMessageHandlerContext context)
        {
            log.Info($"Saga: { Data.Id } ||| Fetched StartOrder: { message.Id }");
            Data.OrderId = message.OrderId;

            var order = new ModelDLL.Order { Id = Data.OrderId, OrderState = ModelDLL.Order.STATE_WAITING_FOR_PAYMENT };
            order.UpdateState(new DatabaseConnection());

            return context.Send(ServiceBusConstants.ACCOUNTANCY_SERVICE_NAME,
                new CheckOrderPayment
                {
                    Id = Data.Id,
                    OrderId = Data.OrderId
                });
        }

        public Task Handle(CheckOrderPaymentResponse message, IMessageHandlerContext context)
        {
            log.Info($"Saga: { Data.Id } ||| Fetched CheckPaymentResponse");

            var order = new ModelDLL.Order { Id = Data.OrderId, OrderState = ModelDLL.Order.STATE_WAITING_FOR_DELIVERY };
            order.UpdateState(new DatabaseConnection());

            return publishCommitedOrder(context);
        }

        public Task Handle(OrderItemReserved message, IMessageHandlerContext context)
        {
            log.Info($"Saga: { Data.Id } ||| Fetched OrderItemReserved: Confirm id: { message.ReservationId }");
            if (message.ReservationId != Data.RecentlyReservationId)
            {
                log.Error("OrderItemReserved - Confirmation ids aren't the same - message is ignored");
                throw new Exception("Unexpected behaviour - Reservation ids aren't the same");
            }

            Data.ItemReservedMessageArrived = true;
            Data.IsItemReserved = true;
            checkIfReservationIsComplete(context);
            return Task.CompletedTask;
        } 

        public Task Handle(OrderItemNotReserved message, IMessageHandlerContext context)
        {
            log.Info($"Saga: { Data.Id } ||| Fetched OrderItemNotReserved. Confirm id: { message.ConfirmationId }");
            if (message.ConfirmationId != Data.RecentlyReservationId)
            {
                log.Error("OrderItemNotReserved - Reservation ids aren't the same - message is ignored");
                throw new Exception("Unexpected behaviour - Confirmation ids aren't the same");
            }

            Data.ItemReservedMessageArrived = true;
            Data.IsItemReserved = false;
            checkIfReservationIsComplete(context);
            return Task.CompletedTask;
        }

        public Task Handle(OrderProviderIsReady message, IMessageHandlerContext context)
        {
            log.Info($"Saga: { Data.Id } ||| Fetched OrderProviderIsReady { message.ReservationId }");
            if (message.ReservationId != Data.RecentlyReservationId)
            {
                log.Error("OrderProviderIsReady - Reservation ids aren't the same - message is ignored");
                throw new Exception("Unexpected behaviour - Reservation ids aren't the same");
            }

            Data.ProviderIsReadyMessageArrived = true;
            Data.IsProviderReady = true;
            checkIfReservationIsComplete(context);
            return Task.CompletedTask;
        }

        public Task Handle(OrderProviderIsNotReady message, IMessageHandlerContext context)
        {
            log.Info($"Saga: { Data.Id } ||| Fetched OrderProviderIsNotReady. Confirm id: { message.ConfirmationId }");
            if (message.ConfirmationId != Data.RecentlyReservationId)
            {
                log.Error("OrderProviderIsNotReady - Reservation ids aren't the same - message is ignored");
                throw new Exception("Unexpected behaviour - Reservation ids aren't the same");
            }

            Data.ProviderIsReadyMessageArrived = true;
            Data.IsProviderReady = false;
            checkIfReservationIsComplete(context);
            return Task.CompletedTask;
        }

        public Task Timeout(RetryConfirmationOrderTimeout state, IMessageHandlerContext context)
        {
            if (state.RetryReservationId != Data.RetryReservationId)
            {
                throw new Exception("Unexpected behaviour - RetryReservationId ids aren't the same");
            }
            log.Info($"Saga: { Data.Id } ||| Retry timeout: { state.Id }");
            return publishCommitedOrder(context);
        }

        private void checkIfReservationIsComplete(IMessageHandlerContext context)
        {
            if (Data.ItemReservedMessageArrived && Data.ProviderIsReadyMessageArrived)
            {
                if (Data.IsProviderReady && Data.IsItemReserved)
                {
                    log.Info($"Saga: { Data.Id } has been marked as completed");

                    var order = new ModelDLL.Order { Id = Data.OrderId, OrderState = ModelDLL.Order.STATE_COMPLETED };
                    order.UpdateState(new DatabaseConnection());

                    MarkAsComplete();
                    context.Publish(new OrderFinished
                    {
                        Id = Data.RecentlyReservationId.Value
                    });
                }
                else
                {
                    reservationRollback(context);
                }
            }
        }

        private void resetReservationFields()
        {
            Data.IsItemReserved = false;
            Data.IsProviderReady = false;
            Data.ProviderIsReadyMessageArrived = false;
            Data.ItemReservedMessageArrived = false;
        }

        private Task publishCommitedOrder(IMessageHandlerContext context)
        {
            var order = ModelDLL.ModelOperations.Select<ModelDLL.Order>(Data.OrderId, new DatabaseConnection());
            if (order == null)
            {
                throw new Exception("Unexpected - saga's order doesn't exist");
            }

            // make sure that there isn't any active confirmation
            if (Data.RecentlyReservationId != null)
            {
                throw new Exception("Recently reservation has not been rollbacked");
                reservationRollback(context);
            }
            Data.RecentlyReservationId = Guid.NewGuid();
            log.Info($"New confirmation id: { Data.RecentlyReservationId }");

            return context.Publish(new OrderCommitted {
                Id = Data.RecentlyReservationId.Value,
                OrderSagaId = Data.Id,
                BookId = order.BookID
            });
        }

        private void reservationRollback(IMessageHandlerContext context)
        {
            log.Warn($"Reservation rollback; ReservationId: { Data.RecentlyReservationId } ");

            resetReservationFields();
            context.Publish(new OrderReservationRollback {
                Id = Data.RecentlyReservationId.Value
            });
            Data.RetryReservationId = Data.RecentlyReservationId.Value;
            Data.RecentlyReservationId = null;

            // setting retry
            var timeout = TimeSpan.FromSeconds(15);  
            RequestTimeout(context, timeout, new RetryConfirmationOrderTimeout { Id = Data.Id,  RetryReservationId = Data.RetryReservationId.Value });
        }              
    }
}
